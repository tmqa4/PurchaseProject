package purchasetestsuite;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pageobjects.AddToCartPopup;
import pageobjects.CheckoutPage;
import pageobjects.ProductPage;
import utility.services.Cоnstants;
import utility.services.Payments;
import utility.services.WaiterService;
import utility.services.WebElementService;

public class PurchaseTestCase extends BaseTestCase {

    String url = "https://www.templatemonster.com/magento-themes/55767.html";

    @Test
    public void purchaseProductBySecondPaymentMethod() {

        WaiterService service = new WaiterService(driver);
        WebElementService service2 = new WebElementService(driver);
        service2.goTo(Cоnstants.TEMPLATE_URL);

        ProductPage template = PageFactory.initElements(driver, ProductPage.class);
//        template.closeBanner();
        template.clickOnPurchaseBtn();

        AddToCartPopup cartPopup = PageFactory.initElements(driver, AddToCartPopup.class);
        cartPopup.selectRandomOffer("OnTemplate");
        cartPopup.selectRandomOffer("OnCart");
        order.setOrderTotal(cartPopup.getOrderTotalPrice());
        cartPopup.clickOnCheckoutBtn();
        service2.switchToLastWindow();
        service.waitURL("checkout");

        CheckoutPage checkout = PageFactory.initElements(driver, CheckoutPage.class);
        checkout.fillUserData(user);
        checkout.payByPaymentMethod(Payments.Stripe);
        service2.switchToLastWindow();
        service.waitURL2("stripe");
    }
}
