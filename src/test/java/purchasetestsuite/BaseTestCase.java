package purchasetestsuite;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import utility.services.NewUser;
import utility.services.Order;
import utility.services.User;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by kirif on 01.09.2017.
 */
public class BaseTestCase {

    public WebDriver driver;

    public User user = new User("properties/user.properties");
    public NewUser newUser = new NewUser("properties/newuser.properties");
    
    Order order = new Order();


    @BeforeMethod
    void init () throws MalformedURLException {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
    }

    @AfterMethod
    void clear(){
        driver.quit();
    }
}

