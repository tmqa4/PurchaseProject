package utility.services;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

import static org.testng.Assert.assertTrue;
import static utility.services.Log.Log;

/**
 * Created by kirif on 27.08.2017.
 */
public class WebElementService {

    private static WebDriver driver;
    static Random random = new Random();

    public WebElementService(WebDriver driver){
        this.driver=driver;
    }

    public static String getCurrentURL(){
        Log.info("Current URL:"+ driver.getCurrentUrl());
        return driver.getCurrentUrl();
    }

    public static void clickOnElement(WebElement element, String elementName, WebDriver driver) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 20);
            wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (TimeoutException ex) {
            return;
        }
        try {
            element.click();
        } catch (NoSuchElementException e) {
            assertTrue(false, "\"" + elementName + "\" was not found on page after timeout.");
        } catch (StaleElementReferenceException e) {
            element.click();
        }
    }

    public static void sendKeys(WebElement element, String elementName, String inputText){
        try {
            element.sendKeys(inputText);
        }
        catch (NoSuchElementException e){
            assertTrue(false, "\"" + elementName + "\" was not found on page after timeout.");
        }
        catch (ElementNotVisibleException e){
            assertTrue(false, "\"" + elementName + "\" was not visible.");
        }
    }

    public void goTo(String url){
        driver.get(url);
    }

    public void switchToLastWindow(){
        driver.getWindowHandles().forEach(driver.switchTo()::window);
    }

    public static String getElementAttribute(WebElement element, String elementName, String attribute){
        String attributeValue = "";
        try {
            //Get value.
            if (element.getAttribute(attribute) != null){
                attributeValue = element.getAttribute(attribute);
            }
            else {
                assertTrue(false,"Attribute \""+attribute+"\" not present.");
            }
            Log.info(attribute + " \"" + elementName +"\" = \"" + attributeValue + "\".");
        }
        catch (NoSuchElementException e){
            assertTrue(false, "\"" + elementName + "\" was not found on page after timeout.");
        }
        catch (ElementNotVisibleException e){
            assertTrue(false, "\"" + elementName + "\" was not visible.");
        }
        return attributeValue;
    }


    public static void chooseRandomElementInList(List <WebElement> list) {
        if(list.isEmpty()){
            Log.warn("Template hasn't lists");
            return;
        }
        int numberOnTemplate = random.nextInt(list.size());
        WaiterService.waitElementClicable(list.get(numberOnTemplate));
        WebElementService.clickOnElement(list.get(numberOnTemplate), "Random element", driver);
        Log.info("Selected random element");
    }
}
