package utility.services;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class User extends NewUser {

    private String password;
    private String email;


    public User(String fileLocation) {
        PropertyReader propertyReader = new PropertyReader(fileLocation);
        this.password = propertyReader.getValue("password");
        this.email = propertyReader.getValue("email");

    }

    public String getEmail() {
        return email;
    }
    public String getPassword() {
        return password;
    }
}
