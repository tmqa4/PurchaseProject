package utility.services;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    private String orderTotal;
    private String orderSubTotal;

    public Order(String fileLocation) {
        PropertyReader propertyReader = new PropertyReader(fileLocation);
        this.orderTotal = propertyReader.getValue("orderTotal");
        this.orderSubTotal = propertyReader.getValue("orderSubTotal");
    }

}

