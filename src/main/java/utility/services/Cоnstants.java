package utility.services;


public class Cоnstants {

    public static final String URL = "https://www.templatemonster.com";
    public static final String TEMPLATE_URL = "https://www.templatemonster.com/joomla-templates/business-responsive-joomla-template-57867.html";

    public static final int PAGE_TIMEOUT = 60;
    public static final int ELEMENT_TIMEOUT = 20;
    public static final int PAGE_ATTEMPT = 30;

}
