package utility.services;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewUser {

    private String userName;
    private String email;
    private String country;
    private String phone;
    private String postalCode;
    private String city;
    private String state;

    public NewUser(String fileLocation) {
        PropertyReader propertyReader = new PropertyReader(fileLocation);
        this.userName = propertyReader.getValue("userName");
        this.email = propertyReader.getValue("email");
        this.country = propertyReader.getValue("country");
        this.phone = propertyReader.getValue("phone");
        this.postalCode = propertyReader.getValue("postalCode");
        this.city = propertyReader.getValue("city");
        this.state = propertyReader.getValue("state");
    }

    public String getUserName() {
        return userName;
    }

    public String getCity() {
        return city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCountry() {
        return country;
    }

    public String getPhone() {
        return phone;
    }

    public String getState() {
        return state;
    }

    public String getEmail() {
        return email;
    }
}
