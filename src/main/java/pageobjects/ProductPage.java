package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static utility.services.WaiterService.waitElementClicable;
import static utility.services.WebElementService.clickOnElement;

/**
 * Created by kirif on 04.09.2017.
 */
public class ProductPage {

    private WebDriver driver;

    @FindBy(xpath = "//button[contains(@id,'preview-add-to-cart-regular')]")
    public WebElement purchaseBtn;

    @FindBy(xpath = "//*[contains(@id,'modalPop')]//div[contains(@class,'icon-close')]")
    public WebElement closeBannerBtn;

    @FindBy(xpath = "//span[contains(@class,'js-close-banner')]")
    public WebElement banner;

    public ProductPage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickOnPurchaseBtn() {
        isPurchaseBtnClicable();
        clickOnElement(purchaseBtn, "Purchase Button", driver);
    }

    public void isPurchaseBtnClicable() {
        waitElementClicable(purchaseBtn);
    }

    public void closeBanner() {
        waitElementClicable(closeBannerBtn);
        clickOnElement(closeBannerBtn, "Close Banner", driver);
        waitElementClicable(purchaseBtn);
    }
}
