package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utility.services.NewUser;
import utility.services.Payments;
import utility.services.User;

import java.util.List;

import static utility.services.Log.Log;
import static utility.services.WaiterService.waitElementClicable;
import static utility.services.WebElementService.clickOnElement;
import static utility.services.WebElementService.sendKeys;

/**
 * Created by kirif on 04.09.2017.
 */
public class CheckoutPage {

    public WebDriver driver;

    @FindBy(xpath = "//*[contains(@id,'checkout-payment-buy')]")
    public List<WebElement> paymentList;

    @FindBy(id = "signin3-form-email")
    public WebElement emailField;

    @FindBy(id = "signin3-new-customer")
    public WebElement emailBtn;

    @FindBy(id = "signin3-form-password")
    public WebElement passField;

    @FindBy(id = "signin3-form-submit")
    public WebElement passBtn;

    @FindBy(id = "billinginfo3-form-fullname")
    public WebElement fullNameField;

    @FindBy(id = "billinginfo3_form_countryiso2_chosen")
    public WebElement countryDropdown;

    @FindBy(xpath = "//*[contains(@class,'country')]//*[@class='chosen-search']//input")
    public WebElement countrySearchField;

    @FindBy(xpath = "//div[contains(@class,'chosen-phone')]//a")
    public WebElement phoneCodeDropdown;

    @FindBy(xpath = "//*[contains(@id,'phone')]//*[@class='chosen-search']//input")
    public WebElement phoneCodetCountryField;

    @FindBy(id = "billinginfo3-form-phone")
    public WebElement phoneField;

    @FindBy(id = "billinginfo3-form-postalcode")
    public WebElement postalCode;

    @FindBy(xpath = "//*[contains(@id,'form_stateiso2_chosen')]")
    public WebElement stateDropdown;

    @FindBy(xpath = "//*[contains(@id,'state')]//*[@class='chosen-search']//input")
    public WebElement stateField;

    @FindBy(id = "billinginfo3-form-cityname")
    public WebElement city;

    @FindBy(id = "billinginfo3-form-submit")
    public WebElement submitButton;

    public CheckoutPage(WebDriver driver) {
        this.driver = driver;
    }

    private void fillNewUserEmail(NewUser user){
        waitElementClicable(emailField);
        sendKeys(emailField, "Email field", user.getEmail());
        emailBtn.click();
        Log.info("Fill email and click Email button");
    }

    private void fillUserEmail(User user){
        waitElementClicable(emailField);
        sendKeys(emailField, "Email field", user.getEmail());
        emailBtn.click();
        Log.info("Fill email and click Email button");
    }

    private void fillUserPassword(User user){
        waitElementClicable(passField);
        sendKeys(passField, "Email field", user.getPassword());
        passBtn.click();
        Log.info("Fill password and click password button");
    }

    private void fillUserName(NewUser user){
        waitElementClicable(fullNameField);
        sendKeys(fullNameField, "FullName", user.getUserName());
    }

    private void fillPhoneNumber(NewUser user){
        waitElementClicable(postalCode);
        sendKeys(postalCode, "Postal code field", user.getPostalCode());
    }

    private void fillZIP(NewUser user){
        waitElementClicable(postalCode);
        sendKeys(postalCode, "Postal code field", user.getPostalCode());
    }

    private void fillCity(NewUser user){
        waitElementClicable(city);
        sendKeys(city, "City", user.getCity());
    }

    public void clickOnSubmitButton(){
        waitElementClicable(submitButton);
        clickOnElement(submitButton, "Submit button",driver);
    }

    public void payByPaymentMethod(Payments paymentMethod){
        waitElementClicable(paymentList.get(1));
        WebElement payment = driver.findElement(By.id("checkout-payment-buy-" + paymentMethod));
        clickOnElement(payment, paymentMethod + " button", driver);
        Log.info("Payed from" + paymentMethod);
    }

    public void fillNewUserData(NewUser user){
        fillNewUserEmail(user);
        fillUserName(user);
        fillPhoneNumber(user);
        fillZIP(user);
        fillCity(user);
    }

    public void fillUserData(User user){
        fillUserEmail(user);
        fillUserPassword(user);
    }

}
