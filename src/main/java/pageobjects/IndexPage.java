package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

import static utility.services.WaiterService.waitElementClicable;
import static utility.services.WebElementService.clickOnElement;

/**
 * Created by kirif on 04.09.2017.
 */
public class IndexPage {

    public static WebDriver driver;

    @FindBy(id = "menu-favorites")
    public WebElement headHeart;

    @FindBy(id = "header-signin-link")
    public WebElement signInLink;

    @FindBy(xpath = "//*[contains(@id,'modalPop')]//div[contains(@class,'icon-close')]")
    public WebElement closeBannerBtn;

    @FindBy(xpath = "//span[contains(@class,'js-close-banner')]")
    public WebElement banner;

    @FindBy(xpath = "//*[contains(@id,'thmb-img')]")
    public List<WebElement> templateList;

    @FindBy(id = "link-tab-new")
    public WebElement newTabLink;

    public IndexPage(WebDriver driver) {
        this.driver = driver;
    }

    public void closeBanner() {
        waitElementClicable(closeBannerBtn);
        clickOnElement(closeBannerBtn, "Close Banner", driver);
    }

}
