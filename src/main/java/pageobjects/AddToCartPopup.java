package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utility.services.WebElementService;

import java.util.List;

import static utility.services.WaiterService.waitElementClicable;
import static utility.services.WaiterService.waitJqueryComplete;
import static utility.services.WebElementService.clickOnElement;
import static utility.services.WebElementService.getElementAttribute;

/**
 * Created by kirif on 04.09.2017.
 */
public class AddToCartPopup {

    private WebDriver driver;

    @FindBy(id = "cart-summary-checkout")
    public WebElement goToCheckoutBtn;

    @FindBy(xpath = ".//*[@class='template-shopping-options upsells']//ul//li//*[contains(@id, 'sc-add-offer-oncart')]")
    public List<WebElement> offerTemplateList;

    @FindBy(xpath = ".//*[@class='template-shopping-options recommended']//ul//li//*[contains(@id, 'sc-add-offer')]")
    public List<WebElement> offerOnCartList;

    @FindBy(xpath = "//*[contains(@class,'js-total-price')]")
    public WebElement orderTotalPrice;

    public AddToCartPopup(WebDriver driver) {
        this.driver = driver;
    }

    public void clickOnCheckoutBtn() {
        waitElementClicable(goToCheckoutBtn);
        clickOnElement(goToCheckoutBtn, "Purchase Button", driver);
    }

    public String getOrderTotalPrice(){
        return getElementAttribute(orderTotalPrice, "Order total price", "data-price");
    }

    public void selectRandomOffer(String offerType){
        waitElementClicable(goToCheckoutBtn);
        switch (offerType){
            case "OnTemplate":
                WebElementService.chooseRandomElementInList(offerTemplateList);
                break;
            case "OnCart":
                WebElementService.chooseRandomElementInList(offerOnCartList);
                break;
            default:
                throw new IllegalArgumentException("Incorrect offer name, choose OnCart or OnTemplate offer name");
        }
        waitJqueryComplete();
    }

}
